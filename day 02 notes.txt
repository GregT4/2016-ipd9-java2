TASK 1

Create project FileSaveRead.

1. Ask user for one line of text input.

2. Open file 'data.txt' in current directory
for writing and write the line of text
user provided.
3. Close the file

4. Open file 'data.txt' one more time for reading, read one line of text from it,
display it back on the console.
5. Close the file.


